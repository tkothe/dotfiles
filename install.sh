#!/usr/bin/env bash

command_exists() {
    type "$1" > /dev/null 2>&1
}

echo "Installing dotfiles."

# see https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
DOTFILES="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


if [ !  -L ~/.dotfiles ] || [ ! -d ~/.dotfiles  ]; then
    echo -e "\\nlinking ~/.dotfiles to current directory"
    ln -s $DOTFILES ~/.dotfiles
fi


echo -e "\\nInitializing submodule(s)"
git submodule update --init --recursive

source install/link.sh

#source install/git.sh

# only perform macOS-specific install
if [ "$(uname)" == "Darwin" ]; then
    echo -e "\\n\\nRunning on OSX"

    source install/homebrew/brew.sh

    source install/osx.sh

    source install/mas.sh
fi

#echo "creating vim directories"
#mkdir -p ~/.vim-tmp


echo "Done. Reload your terminal."
