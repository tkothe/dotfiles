#!/bin/bash

if test ! "$(which brew)"; then
    echo "Installing homebrew"
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> ~/.zprofile
    eval "$(/opt/homebrew/bin/brew shellenv)"
else
    echo "homebrew already installed"
fi

echo -e "\\n\\nInstalling homebrew packages..."

formulas=(
# TODO: complete list of formulae
    awscli
#    bash-completion
    curl
    colima
#    exiftool
#    ffmpeg
    fzf
    git
    google-cloud-sdk
    gpg
#    jenv
    jq
    'macvim --with-override-system-vim --with-lua'
    helm
    kubernetes-cli
    kubectx
#    minikube
#    helm
#    libdvdcss
    mas
    node
#    nvm
    pwgen
    reattach-to-user-namespace # don't remove or you might break tmux https://superuser.com/a/454827 
#    rbenv
    shellcheck
#    telnet
#    terraform
    tmux
    wget
    zsh-completions
)

taps=(
   "homebrew/cask-fonts"
)

casks=(
    bartender
#    diffmerge
#    docker
    chatgpt
    cursor
    calibre
    font-hack
    grandperspective
    handbrake
    google-chrome
    google-cloud-sdk
    iterm2
    jdownloader
    keepingyouawake
    monitorcontrol
    signal
    rectangle
#    virtualbox # not available for Apple Silicon
    vscodium
    qlvideo 
    quicklook-csv
    quicklook-json
    rambox
    vlc
    xbar
)

for formula in "${formulas[@]}"; do
    formula_name=$( echo "$formula" | awk '{print $1}' )
    if brew list "$formula_name" > /dev/null 2>&1; then
        echo "$formula_name already installed... skipping."
    else
        brew install "$formula"
    fi
done

for tap in "${taps[@]}"; do
    if brew tap | grep "$tap" > /dev/null 2>&1; then
        echo "$tap already tapped... skipping."
    else
        brew tap "$tap"
    fi
done

for cask in "${casks[@]}"; do
    if brew list --cask "$cask" > /dev/null 2>&1; then
        echo "$cask already installed... skipping."
    else
        brew install --cask "$cask"
    fi
done
