#!/usr/bin/env sh

COLOR_RED="\033[1;31m"

# a lot of options are found here
# https://gist.github.com/bradp/bea76b16d3325f5c47d4
# https://github.com/nicknisi/dotfiles/blob/b9e1d2af05251392fd88d88861470d3acb52790d/install.sh#L202
# https://github.com/herrbischoff/awesome-macos-command-line/blob/master/README.md
# https://marketmix.com/de/mac-osx-umfassende-liste-der-terminal-defaults-commands/

echo -e "\n\nSetting OS X settings"
echo "=============================="

echo "Show hidden files by default"
defaults write com.apple.Finder AppleShowAllFiles -bool true

echo "Show Status bar in Finder"
defaults write com.apple.finder ShowStatusBar -bool true

echo "Set Dock to auto-hide and reduce the auto-hiding delay"
defaults write com.apple.dock autohide -bool true
defaults write com.apple.dock autohide-delay -float 0
defaults write com.apple.dock autohide-time-modifier -float 0.2

echo "Set Mouse speed"
defaults write -g com.apple.mouse.scaling 2.2

echo "Set key repead for VSCodium"
# https://github.com/VSCodeVim/Vim#mac
defaults write com.vscodium ApplePressAndHoldEnabled -bool false

echo "${COLOR_RED}Please restart affected applications (Finder, Dock)"
