# Troubleshooting
This is a list of issues I have run into in the past.


<!-- vim-markdown-toc GFM -->

* [nvm is not compatible with the prefix option](#nvm-is-not-compatible-with-the-prefix-option)
* [Powerline font characters disappear in tmux](#powerline-font-characters-disappear-in-tmux)

<!-- vim-markdown-toc -->

## nvm is not compatible with the prefix option

When receiving an error message like
```
nvm is not compatible with the npm config "prefix" option:
   currently set to "/usr/local/"
   Run `npm config delete prefix` or `nvm use --delete-prefix v6.2.2` to unset it.
```

simply delete the prefix and set it to `$NVM_DIR/versions/node/<node_version>` like this
```
$ npm config delete prefix
$ npm config set prefix $NVM_DIR/versions/node/v6.2.2
```

As a side effect, this also results in my tmux shell being launched in another shell, so when I exit one of my tmux shells (^D), I am stil left with a shell that is just a plain prompt like `bash-3.2 $`.


[Stackoverflow reference](https://stackoverflow.com/questions/34718528/nvm-is-not-compatible-with-the-npm-config-prefix-option)


## Powerline font characters disappear in tmux

I ran into a situation where suddenly my Powerline characters disappeared from both my tmux status bar as well as my vim status bar. Running `tmux -u`, i.e. explicitly enabling UTF-8 support fixed it but I wanted it properly configured rather than explicitly set when launching. Even though `tmux-sensible` should set `set -g status-utf8`, I did not get UTF-8 support either. Turns out, that the according to the [tmux FAQ](https://github.com/tmux/tmux/wiki/FAQ#how-do-i-use-utf-8), also `LANG` needs to be set to a `UTF-8` value (e.g. in my case `C.UTF-8`). I added this to my `.bash_profile`.
