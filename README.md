# dotfiles
My dotfiles (and eventually essentially my whole computer configuration).

This setup is based on the nice [dotfiles repo by nicknisi](https://github.com/nicknisi/dotfiles).

## Installation

(this is the main part that is copied from Nick Nisi. The instructions below are copied verbatim.)

clone this repo and run
```
$ install.sh
```

 It will install all symbolic links into your home directory. Every file with a .symlink extension will be symlinked to the home directory with a . in front of it. As an example, `vimrc.symlink` will be symlinked in the home directory as `~/.vimrc`. Then, this script will create a `~/.vim-tmp` directory in your home directory, as this is where vim is configured to place its temporary files. Additionally, all files in the $DOTFILES/config directory will be symlinked to the `~/.config/` directory for applications that follow the XDG base directory specification, such as neovim.

**TODO:** THE FOLLOWING IS YET TO BE CONFIGURED BY ME

Next, the install script will perform a check to see if it is running on an OSX machine. If so, it will install Homebrew if it is not currently installed and will install the homebrew packages listed in `brew.sh`. Then, it will run `osx.sh` and change some OSX configurations. This file is pretty well documented and so it is advised that you read through and comment out any changes you do not want. Next, nginx (installed from Homebrew) will be configured with the provided configuration file. If a `nginx.conf` file already exists in  `/usr/local/etc`, a backup will be made at `/usr/local/etc/nginx/nginx.conf.original`.

## bash
Bash history is saved after every command and history file size is increased as suggested [this FAQ](http://mywiki.wooledge.org/BashFAQ/088). This is accomplished by the following lines in `.bashrc` (or rather `bashrc.symlink` in this repository)

```
HISTFILESIZE=400000000
HISTSIZE=10000
PROMPT_COMMAND="history -a"

shopt -s histappend
```

## Terminal 

* set Font to Hack

it appears that after having base16-shell installed and .zshrc set up correctly (see https://github.com/chriskempson/base16-shell#bashzsh ), one needs to run `base16_chalk` once, in order to configure Terminal and vim correctly.


## iTerm settings
in your Default profile 
* set non-ASCII font to font that supports Powerline Symbols (e.g. Hack font, see above)
* disable "Draw bold text in bold font" setting
* get base16-chalk color scheme from https://github.com/martinlindhe/base16-iterm2 and import and use the 256 color version


## tmux

**TODO:** notes on how I use/configure tmux

tmux plugin manager (tpm) should be part of this repo now
install all Plugins see https://github.com/tmux-plugins/tpm#installing-plugins
> Press prefix + I (capital i, as in Install) to fetch the plugin.



## vim

**TODO:** notes on how I use/configure vim

## more TODOs
- [ ] include alpine configuration
- [ ] evaluate whether I want to use fzf (if so, check out what nicknisi's install script does)
- [ ] evaluate whether I want to switch to zsh
- [ ] a good reason to switch to zsh is this plugin that does fun things with the Apple Touchbar: https://github.com/iam4x/zsh-iterm-touchbar
- [ ] evaluate whether I want to swtich from macvim to neovim
- [ ] update Scala integration according to [this](https://medium.com/@alandevlin7/neovim-scala-f392bcd8b7de) - possibly switching to neovim
- [ ] add termtile installation https://github.com/apaszke/termtile or decide what to do with it
- [ ] evaluate whether a few presets using tmuxinator make sense
- [ ] add a warning to install script that if reattach-to-user-namespace is not found tmux will not execute .bash_profile for its shells
- [ ] add customized spectacle app shortcuts
- [ ] possibly try [Powerlevel9k](https://medium.freecodecamp.org/how-you-can-style-your-terminal-like-medium-freecodecamp-or-any-way-you-want-f499234d48bc) when switching to ZSH 
- [ ] more inspiration [here](https://medium.com/@caulfieldOwen/youre-missing-out-on-a-better-mac-terminal-experience-d73647abf6d7) including e.g. [Spaceship ZSH](https://github.com/denysdovhan/spaceship-prompt) 
- [ ] find a way to manage a .gitconfig that would work across private/work machine and set up pretty git log aliases like [here](https://stackoverflow.com/questions/1057564/pretty-git-branch-graphs)
- [ ] add imgcat fix for tmux [like this](https://gist.github.com/krtx/533d33d6cc49ecbbb8fab0ae871059ec)

## other notes

Use Hack as the font

http://sourcefoundry.org/hack/

== set CapsLock to be Control

== use base16-shell


== Awesome macOS Command Line
[Awesome macOS Command Line](https://github.com/herrbischoff/awesome-macos-command-line/blob/master/README.md) curated awesome list on GitHub.

